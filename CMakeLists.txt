cmake_minimum_required(VERSION 3.5)

project(variant)

set(CMAKE_CXX_STANDARD 17)

include_directories(include)
add_compile_options(-O4 -g)

add_executable(vartest test/vartest.cc)
#target_link_libraries(vartest tcmalloc)

add_executable(read_json test/read_json.cc)
#target_link_libraries(read_json tcmalloc)

