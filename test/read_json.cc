#include <variant.hpp>
#include <fstream>
#include <json.hpp>
#include <chrono>
#include <type_traits>
template <typename T>
void run(const char *fn) {
   auto start1 = std::chrono::system_clock::now();
   T v;
   if constexpr(std::is_same_v<T,variant>) {
       T::parse(fn,v);
     }
   else {
     std::ifstream in(fn);
     in >> v;

   }
   auto end1 = std::chrono::system_clock::now();
   std::chrono::duration<double> m1 = end1-start1;
   std::cout << "read json file " << m1.count() <<  std::endl;
   auto start2 = std::chrono::system_clock::now();
   std::vector<uint8_t> msgpack=T::to_msgpack(v);
   auto end2 = std::chrono::system_clock::now();
   std::chrono::duration<double> m2 = end2-start2;
   std::cout << "msgpack size: " << msgpack.size() << std::endl;
   std::cout << "to msgpack " << m2.count() << std::endl;
   auto start3 = std::chrono::system_clock::now();
   T w=T::from_msgpack(msgpack);
   auto end3 = std::chrono::system_clock::now();
   std::chrono::duration<double> m3 = end3-start3;
   std::cout << "from msgpack " << m3.count() << std::endl;
   if constexpr(std::is_same_v<T,variant>) {
       //   w.dump();

   }
   
}


int main(int argc,char *argv[]) {
  run<nlohmann::json>(argv[1]);
  run<variant>(argv[1]);
}
